require "rubygems"
require 'faye'
require 'faye/redis'
require 'redis'
require "pg"
require "open-uri"
require "net/http"
require "nokogiri"
require "active_record"
require File.expand_path('../config/initializers/faye_token.rb', __FILE__)

$redis = Redis.new
class ServerAuth	
  def incoming(message,callback)
    if message["data"]
#      puts message["data"]["data"]
#      puts $redis.get(message["data"]["data"])    
#      message["data"] = {:data=> $redis.get(message["data"]["data"]).split(";")}.to_json
#      puts callback.call(message)
    end
#    if !message["data"]
#      puts callback.call(message)
#    end    	
  end
end  

ActiveRecord::Base.establish_connection(
:adapter => "postgresql",
:host => "localhost",
:username => "postgres",
:password => "a3d1sw2qwe",
:database => "postgres"
)

class Word < ActiveRecord::Base
  has_and_belongs_to_many :rsses
end

class Rss < ActiveRecord::Base
  has_and_belongs_to_many :words
end

class RssesWords < ActiveRecord::Base
  belongs_to :word
  belongs_to :rss
end

class Dictionary < ActiveRecord::Base
end

def news_cat(url) 
  @doc_arr = []	
  @doc = Nokogiri::XML(open(url))
  @doc.xpath("//channel").map{|node|
    @doc_arr = node.xpath("//title").to_s.split("</title><title>")
  }
  s = @doc_arr.size - 1
  @doc_arr = @doc_arr[2..s]
  if !@doc_arr.empty?
    @doc_arr[s-2] = @doc_arr[s-2][0..@doc_arr[s-2].size-9]
  end
  return @doc_arr.join(";")
end


Faye::WebSocket.load_adapter('thin')
faye_server = Faye::RackAdapter.new(:mount => '/faye', 
				    :timeout => 25,
				    )
faye_server.add_extension(ServerAuth.new)
run faye_server


def down_ch(str)
  str.mb_chars.downcase.wrapped_string
end

def count_word_in_title(key,title)
  tit = title.split(" ")
  tit.each{|t|
    res = res_tmp = down_ch(t)
    if !Dictionary.where(value: res[0...t.size-1]).empty? || !Dictionary.where(value: res[0...t.size-2]).empty?
      res = Dictionary.where(value: res_tmp[0...t.size-2]).first.value if !Dictionary.where(value: res_tmp[0...t.size-2]).empty? && t.size-2 > 2
      res = Dictionary.where(value: res_tmp[0...t.size-1]).first.value if !Dictionary.where(value: res_tmp[0...t.size-1]).empty? && t.size-1 > 2
    end
    if t.size > 3 && t.to_i == 0
      if Word.where(date: Time.now.beginning_of_day,title: res).empty?        
        a = Word.new
        a.count = 0
        a.date = Time.now.beginning_of_day
        a.title = res
        a.save!
        RssesWords.create(word_id: a.id,rss_id: Rss.find_by_rss_index(key).id,count: 0)
      else
        a = Word.where(date: Time.now.beginning_of_day,title: res).first
        if a.rss_ids.include?(Rss.where(rss_index: key).first.id)
          rw = RssesWords.where(word_id: a.id,rss_id: Rss.where(rss_index: key).first.id).first
          rw.count = rw.count + 1
          rw.save!
        else
          RssesWords.create(word_id: a.id,rss_id: Rss.find_by_rss_index(key).id,count: 0)
        end
        a.count = a.count + 1
      end
      a.save!
    end 
  }
end

def count_word_in_day(key,new_val)
  val = $redis.get(key)
  val = down_ch(val)
  new_val.split(";").each{|t|
    small_t = down_ch(t)
  #  puts "-----------------------------------------------"
  #  puts val
  #  puts "-----------------------------------------------"
  #  puts new_val
  #  puts "-----------------------------------------------"
    if !val.include?(small_t)
      count_word_in_title(key,small_t)
    end
  }       
end

Thread.new do 	
  loop do
    puts "I am working now"	  
    Rss.all.each{|e|
      begin 
        puts e.title
        puts e.rss_index
        puts news_cat(e.rss_index)
        $redis.set(e.rss_index, news_cat(e.rss_index))
        count_word_in_day(e.rss_index,news_cat(e.rss_index))
      rescue Exception => e  
        puts e.message  
        puts e.backtrace.inspect 
      end
    }
    puts "I am done"    
    sleep 1800
  end
end

