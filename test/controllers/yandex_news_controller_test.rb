require 'test_helper'

class YandexNewsControllerTest < ActionController::TestCase
  setup do
    @yandex_news = yandex_news(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:yandex_news)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create yandex_news" do
    assert_difference('YandexNews.count') do
      post :create, yandex_news: {  }
    end

    assert_redirected_to yandex_news_path(assigns(:yandex_news))
  end

  test "should show yandex_news" do
    get :show, id: @yandex_news
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @yandex_news
    assert_response :success
  end

  test "should update yandex_news" do
    patch :update, id: @yandex_news, yandex_news: {  }
    assert_redirected_to yandex_news_path(assigns(:yandex_news))
  end

  test "should destroy yandex_news" do
    assert_difference('YandexNews.count', -1) do
      delete :destroy, id: @yandex_news
    end

    assert_redirected_to yandex_news_index_path
  end
end
