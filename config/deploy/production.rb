# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
role :app, %w{root@s05657572.fastvps-server.com}
role :web, %w{root@s05657572.fastvps-server.com}
role :db,  %w{root@s05657572.fastvps-server.com}


# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

server 's05657572.fastvps-server.com', user: 'root', roles: %w{web app}, my_property: :my_value


#namespace :deploy do
#  task :restart_rails do
#    run "kill -9 `cat /home/news_parser/current/tmp/pids/server.pid`"
#    run "cd /home/news_parser/current/ && RAILS_ENV=production rails s &"
#  end
#end

# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult[net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start).
#
# Global options
# --------------
#  set :ssh_options, {
#    keys: %w(/home/rlisowski/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
#
# And/or per server (overrides global)
# ------------------------------------
# server 'example.com',
#   user: 'user_name',
#   roles: %w{web app},
#   ssh_options: {
#     user: 'user_name', # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: 'please use keys'
#   }
