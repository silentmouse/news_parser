Rails.application.routes.draw do

  resources :posts

  resources :search do
    collection do
      get 'search_word'
    end
  end

  resources :category_news

  resources :dictionaries

  resources :words do
    collection do
      get 'words_in_day'
      get 'words_in_rss'
      get 'charts'
    end
  end

  resources :frequencies
  resources :rsses

  resources :yandex_news do
    collection do
      get 'news_category' 
    end
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  match "/404" => "errors#error_404", via: [ :get, :post, :patch, :delete ]
  match "/500" => "errors#error_500", via: [ :get, :post, :patch, :delete ]
  # You can have the root of your site routed with "root"
  root 'search#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
