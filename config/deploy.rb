#config valid only for current version of Capistrano

#default_run_options[:shell] = '/bin/bash'
lock '3.3.5'
set :application, 'news_parser'
set :repo_url, 'git@bitbucket.org:silentmouse/news_parser.git'
# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call
# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/news_parser'
set :user, "root"
# Default value for :scm is :git
set :scm, :git
set :rails_env, :production
# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug
# Default value for :pty is false
set :pty, true
# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml')

# Default value for linked_dirs is []
 set :linked_dirs, fetch(:linked_dirs, []).push('bin', 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }
SSHKit.config.command_map[:rails_s] = "RAILS_ENV=production"
# Default value for keep_releases is 5
# set :keep_releases, 5


namespace :deploy do
  
  task :restart_faye do
    on roles(:web) do 
      execute 'sh /home/news_parser/current/rails_s.sh'
    end
  end

  task :restart_nginx do
    on roles(:web) do 
      execute 'service nginx restart'
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
#       execute " && ", "RAILS_ENV=production rails s &"
      within release_path do
        execute :rake, 'tmp:cache:clear'
#        execute :sh, './rails_s.sh'
      end
    end
  end

end

after "deploy", "deploy:restart"
after "deploy", "deploy:stop"
after "deploy", "deploy:start"
after "deploy", "deploy:restart_nginx"
