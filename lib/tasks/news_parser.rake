require 'add_dictionary'
require 'add_words'
require 'update_words'
namespace :news_parser do

  desc 'add to_dictionary'
  task add_to_dictionary: :environment do
    TaskDictionary::add_dictionary
  end

  desc 'add to words'
  task add_words: :environment do
    AddWordsClass::add_words
  end

  desc 'update words date'
  task update_words: :environment do
    UpdateWordsClass::update_words
  end
end

