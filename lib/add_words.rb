module AddWordsClass
  class << self
    $redis = Redis.new

    def news_cat(url) 
      @doc_arr = []	
      @doc = Nokogiri::XML(open(url))
      @doc.xpath("//channel").map{|node|
        @doc_arr = node.xpath("//title").to_s.split("</title><title>")
        @doc_arr_link = node.xpath("//link").to_s.split("</link><link>")
      }
      s = @doc_arr.size - 1
      @doc_arr = @doc_arr[2..s]     
      if !@doc_arr.empty?
        @doc_arr[s-2] = @doc_arr[s-2][0..@doc_arr[s-2].size-9]
      end
      s_l = @doc_arr_link.size - 1
      @doc_arr_link = @doc_arr_link[2..s_l]     
      if !@doc_arr_link.empty?
        @doc_arr_link[s_l-2] = @doc_arr_link[s_l-2][0..@doc_arr_link[s_l-2].size-7]
      end
      $redis.set("#{url}_link", @doc_arr_link.join(";"))
      return @doc_arr.join(";")
    end

    def down_ch(str)
      str.mb_chars.downcase.wrapped_string
    end

    def count_word_in_title(key,title)
      tit = title.split(" ")
      tit.each{|t|
        res = res_tmp = down_ch(t)
        if (!Dictionary.where(value: res[0...t.size-1]).empty? || !Dictionary.where(value: res[0...t.size-2]).empty?) && res != "россия" && res != "путин"
          res = Dictionary.where(value: res_tmp[0...t.size-2]).first.value if !Dictionary.where(value: res_tmp[0...t.size-2]).empty? && t.size-2 > 2
          res = Dictionary.where(value: res_tmp[0...t.size-1]).first.value if !Dictionary.where(value: res_tmp[0...t.size-1]).empty? && t.size-1 > 2
        end
        next if ["год","компания","компании","из-за"].include?(res)
        res = res.delete("=;:_@#%$()*\"\'»«")
        if t.size > 3 && t.to_i == 0
          if Word.where(date: Time.now.beginning_of_day,title: res).empty?        
            a = Word.new
            a.count = 0
            a.date = Time.now.beginning_of_day
            a.title = res
            a.save!
            RssesWords.create(word_id: a.id,rss_id: Rss.find_by_rss_index(key).id,count: 0)
         else
           a = Word.where(date: Time.now.beginning_of_day,title: res).first
           if a.rss_ids.include?(Rss.where(rss_index: key).first.id)
             rw = RssesWords.where(word_id: a.id,rss_id: Rss.where(rss_index: key).first.id).first
             rw.count = rw.count + 1
             rw.save!
           else
             RssesWords.create(word_id: a.id,rss_id: Rss.find_by_rss_index(key).id,count: 0)
           end
           a.count = a.count + 1
         end
         a.save!
        end 
      }
    end

    def count_word_in_day(key,new_val)
      val = $redis.get(key)
      val = down_ch(val)
      new_val.split(";").each{|t|
        small_t = down_ch(t)
        puts small_t
        if !val.include?(small_t)
          count_word_in_title(key,small_t)
        end
       }       
    end

    def add_words
      puts "I am working now"	  
      Rss.all.each{|e|
        begin 
          puts e.title
          puts e.rss_index
          puts news_cat(e.rss_index)
          if $redis.get(e.rss_index) != nil
            count_word_in_day(e.rss_index,news_cat(e.rss_index))
          end
          $redis.set(e.rss_index, news_cat(e.rss_index))
        rescue Exception => e  
          puts e.message  
          puts e.backtrace.inspect 
        end
      }
      puts "I am done in #{Time.now}"    
    end
  end
end
