module UpdateWordsClass
  class << self
    def update_words
      Word.all.each{|t|
        puts "Word id eq #{t.id}"
        if t.date.hour == 5
          t.update_column :date , t.date - 8.hour
        end
        if t.date.hour == 4
          t.update_column :date , t.date - 7.hour
        end
      }
    end
  end
end
