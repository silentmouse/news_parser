class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

 def two_arrays_to_one(url)
    @first_arr = $redis.get(url).split(";")
    @second_arr = $redis.get("#{url}_link").split(";")
    @result_arr = {}
    for i in 0...@first_arr.size
      @result_arr[@first_arr[i]]=@second_arr[i]
    end
    @result_arr
  end

end
