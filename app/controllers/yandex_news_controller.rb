class YandexNewsController < ApplicationController
  before_action :set_yandex_news, only: [:show, :edit, :update, :destroy]

  # GET /yandex_news
  # GET /yandex_news.json
  def index
    @rating_words = Word.where(date: Time.now.beginning_of_day).order("count DESC").first(10)
    @categories = CategoryNews.all.map{|t| {t.title=>t.rsses.map{|e| {e.title=>e.rss_index}}}}
    @res_main_rus = two_arrays_to_one("http://news.yandex.ru/index.rss")
    @res_main_ua = two_arrays_to_one("http://news.yandex.ua/index.rss")
  end

  def news_category
    @doc_arr = {}
    @doc_arr = two_arrays_to_one(params[:url])
    render layout: false
  end

  #  GET /yandex_news/1
  # GET /yandex_news/1.json
  def show
  end

  # GET /yandex_news/new
  def new
    @yandex_news = YandexNews.new
  end

  # GET /yandex_news/1/edit
  def edit
  end
#
  # POST /yandex_news
  # POST /yandex_news.json
  def create
    @yandex_news = YandexNews.new(yandex_news_params)

    respond_to do |format|
      if @yandex_news.save
        format.html { redirect_to @yandex_news, notice: 'Yandex news was successfully created.' }
        format.json { render :show, status: :created, location: @yandex_news }
      else
        format.html { render :new }
        format.json { render json: @yandex_news.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /yandex_news/1
  # PATCH/PUT /yandex_news/1.json
  def update
    respond_to do |format|
      if @yandex_news.update(yandex_news_params)
        format.html { redirect_to @yandex_news, notice: 'Yandex news was successfully updated.' }
        format.json { render :show, status: :ok, location: @yandex_news }
      else
        format.html { render :edit }
        format.json { render json: @yandex_news.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /yandex_news/1
  # DELETE /yandex_news/1.json
  def destroy
    @yandex_news.destroy
    respond_to do |format|
      format.html { redirect_to yandex_news_index_url, notice: 'Yandex news was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_yandex_news
      @yandex_news = YandexNews.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def yandex_news_params
      params[:yandex_news]
    end
end
