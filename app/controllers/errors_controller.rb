class ErrorsController < ApplicationController

  def error_404
    redirect_to root_path
  end

  def error_500
  end
end
