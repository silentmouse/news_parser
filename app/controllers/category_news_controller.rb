class CategoryNewsController < ApplicationController
  before_action :set_category_news, only: [:show, :edit, :update, :destroy]

  # GET /category_news
  # GET /category_news.json
  def index
    @category_news = CategoryNews.all
  end

  # GET /category_news/1
  # GET /category_news/1.json
  def show
  end

  # GET /category_news/new
  def new
    @category_news = CategoryNews.new
  end

  # GET /category_news/1/edit
  def edit
  end

  # POST /category_news
  # POST /category_news.json
  def create
    @category_news = CategoryNews.new(category_news_params)

    respond_to do |format|
      if @category_news.save
        format.html { redirect_to @category_news, notice: 'Category news was successfully created.' }
        format.json { render :show, status: :created, location: @category_news }
      else
        format.html { render :new }
        format.json { render json: @category_news.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /category_news/1
  # PATCH/PUT /category_news/1.json
  def update
    respond_to do |format|
      if @category_news.update(category_news_params)
        format.html { redirect_to @category_news, notice: 'Category news was successfully updated.' }
        format.json { render :show, status: :ok, location: @category_news }
      else
        format.html { render :edit }
        format.json { render json: @category_news.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /category_news/1
  # DELETE /category_news/1.json
  def destroy
    @category_news.destroy
    respond_to do |format|
      format.html { redirect_to category_news_index_url, notice: 'Category news was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category_news
      @category_news = CategoryNews.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_news_params
      params.require(:category_news).permit(:title)
    end
end
