class WordsController < ApplicationController
  before_action :set_word, only: [:show, :edit, :update, :destroy]

  # GET /words
  # GET /words.json
  def index
    @words = Word.where(date: Time.now.beginning_of_day).order("count DESC").limit(20)
    @categories = CategoryNews.all
    @time = [7,6,5,4,3,2,1,0].map{|t| Time.now.beginning_of_day - t.day}
    @words_in_dates = @time.map{|t|
      Word.where(date: t).order("count DESC").limit(20)
    }
    @rss_titles = Rss.all
  end

  # GET /words/1
  # GET /words/1.json
  def show
  end

  def charts
    @categories = CategoryNews.all
    @time_search = params[:time] == nil ? Time.now.beginning_of_day : params[:time].to_time
    @time = [7,6,5,4,3,2,1,0].map{|t| (Time.now.beginning_of_day - t.day)}
    if params[:category_news] 
      @words_arr = Word.where(date: @time_search).joins(:rsses).where("rsses.category_news_id = #{params[:category_news]}").uniq.order("count DESC").limit(8)
    else
      @words_arr = Word.where(date: @time_search).order("count DESC").limit(8)
    end
    @words = @words_arr.map{|t| 
      result_arr = {}
      @time.each{|r|
        result_arr[r]= Word.where(title: t.title,date: r).empty? ? 0 : Word.where(title:t.title,date: r).first.count
      }
      [t.title,result_arr]
    }
    @time_date = @time.map{|t| t}
  end

  def words_in_day
    @words = Word.where(date: params[:date].to_time.beginning_of_day).order("count DESC").limit(40)
    render layout: false
  end

  def words_in_rss
    @time = [7,6,5,4,3,2,1,0].map{|t| Time.now.beginning_of_day - t.day}
    @words_in_dates = @time.map{|t|
      Word.where(date: t).joins(:rsses).where("rsses.category_news_id = #{params[:category_news_id]}").uniq.order("count DESC").limit(20)
    }
    render layout: false
  end

  # GET /words/new
  def new
    @word = Word.new
  end

  # GET /words/1/edit
  def edit
  end

  # POST /words
  # POST /words.json
  def create
    @word = Word.new(word_params)

    respond_to do |format|
      if @word.save
        format.html { redirect_to @word, notice: 'Word was successfully created.' }
        format.json { render :show, status: :created, location: @word }
      else
        format.html { render :new }
        format.json { render json: @word.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /words/1
  # PATCH/PUT /words/1.json
  def update
    respond_to do |format|
      if @word.update(word_params)
        format.html { redirect_to @word, notice: 'Word was successfully updated.' }
        format.json { render :show, status: :ok, location: @word }
      else
        format.html { render :edit }
        format.json { render json: @word.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /words/1
  # DELETE /words/1.json
  def destroy
    @word.destroy
    respond_to do |format|
      format.html { redirect_to words_url, notice: 'Word was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_word
      @word = Word.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def word_params
      params.require(:word).permit(:date, :title, :count)
    end
end
