class SearchController < ApplicationController

  def index
  end

  def search_word
    @time_from = params[:time_from].nil? || params[:time_from] == "" ? Time.now.beginning_of_day - 1.week : params[:time_from].to_time
    @time_to = params[:time_to].nil? || params[:time_to] == "" ? Time.now.beginning_of_day : params[:time_to].to_time
    #@time = @time_search = params[:time].nil? || params[:time] == "" ? Time.now.beginning_of_day : params[:time].to_time
    @words_arr = params[:word].delete(",;:@$%()\"\'«»").split(" ")
    @result_words = []
    @result_words_new = []
    @words_arr.each{|t|
    if params[:filter] == "true"
      @search_word = t.mb_chars.downcase.wrapped_string
    else
      @search_word = "%#{t[0...t.size-1]}%".mb_chars.downcase.wrapped_string
    end
      @result_words << Word.select("title, sum(count) as sum_count").where("title like ? and date >= ? and date <= ?",@search_word, @time_from,@time_to).group("words.title").order("sum_count DESC")
      @result_words_new << Word.where("title like ? and date >= ? and date <= ?",@search_word,@time_from,@time_to).map{|t| t.count}
    }
    @result_words = @result_words.flatten.sort_by{|t| t.sum_count}.reverse
    @expected_array = []
    @result_words_new.each{|t|
      @expected_array << expected_value(t)
    }
    @new_array_for_d = []
    for i in 0...@expected_array.size
      @new_array_for_d << expected_value(@result_words_new[i].map{|t| t**2}) - @expected_array[i] ** 2
    end
    #@new_array_for_d_res = []
    #@new_array_for_d.each{|t|
    #  @new_array_for_d_res << expected_value(t)
    #}
    @time_arr = []
    while @time_to != @time_from
      @time_arr << @time_to
      @time_to = @time_to - 1.day
    end
    @time_arr = @time_arr.reverse
    @words = @result_words.first(8).map{|t|
      result_arr = []
      @time_arr.each{|r|
        result_arr << (Word.where(title: t.title,date: r).empty? ? 0 : Word.where(title:t.title,date: r).first.count)
      }
      [t.title,result_arr]
    }
    #@partial_selection = @result_words.first(8).map{|t|
    #  result_arr = {}
    #  20.times{
    #    rand_time_val = rand_time(Time.new(2015,02,28)).beginning_of_day
    #    result_arr[rand_time_val.to_s] = (Word.where(title: t.title,date: rand_time_val).empty? ? 0 : Word.where(title:t.title,date: rand_time_val).first.count)
    #  }
    #  [t.title,result_arr]
    #}
    @partial_selection_tmp = []
    size_a = @words.size
    size_b = @words[0][1].size
    for i in 0...size_b
      s = 0
      for j in 0...size_a
        s = s + @words[j][1][i]
      end
      @partial_selection_tmp << (s/2) * 8
    end  
    @partial_selection = ["korrelation",@partial_selection_tmp]
    @sum_middle_arr = []
    if params[:filter] == "true"
      @words.each{|t|
        #@sum_middle = 0
        #t[1].each{|e|
        #  @sum_middle = @sum_middle + e
        #}
        @sum_middle_arr_1 = []
        t[1].each{|e|
         @sum_middle_arr_1 << @expected_array.first
        }
       @sum_middle_arr << t[0] + " Mат ожидание"
       @sum_middle_arr << @sum_middle_arr_1
      } 
    end
    @words_changed = []
    if params[:filter] == "true"
      tmp = 0
      @words.each{|word|
        s = 0
        @words_changed << [word[0],word[1].map{|t| res = tmp - t; s = s + res.abs; tmp = t; res * (-1)}, (s.to_f/word[1].size).round(5)]
      }
    end
    @middle_in_last_10 = @words.map{|t|
      middle(t[1].last(10))
    }  
    @words = @words << @sum_middle_arr if !@sum_middle_arr.empty?
    @time_date = @time_arr.map{|t| t}
    render layout: false
  end
  
  def expected_value(a)
    b={}
    a.each{|t|
      if b[t] == nil
        b[t] = 1
      else
        b[t] = b[t] + 1
      end
    }
    puts b
    size = a.size.to_f
    b.each{|t| b[t[0]] = (b[t[0]] / size).round(4)}
    puts b
    res = 0
    b.each{|t| res = res + t[0] * t[1]}
    res
  end
 
  def middle(a)
    s = 0
    a.each{|t|
      s = s + t
    }
    (s.to_f/a.size.to_f).round(5)
  end

  def rand_time(from, to=Time.now)
    Time.at(rand_in_range(from.to_f, to.to_f))
  end

  def rand_in_range(from, to)
    rand * (to - from) + from
  end

  

end
