class RssesController < ApplicationController
  before_action :set_rss, only: [:show, :edit, :update, :destroy]

  # GET /words
  # GET /words.json
  def index
  end

  def show
    @news = two_arrays_to_one(@rss.rss_index)
    #@news = @news.split(";")
    @words = @rss.words.where(date: Time.now.beginning_of_day)
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rss
      @rss = Rss.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rss_params
      params.require(:rss).permit(:title, :rss_index)
    end
end
