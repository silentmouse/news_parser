json.array!(@frequencies) do |frequency|
  json.extract! frequency, :id, :date, :title, :count
  json.url frequency_url(frequency, format: :json)
end
