json.array!(@words) do |word|
  json.extract! word, :id, :date, :title, :count
  json.url word_url(word, format: :json)
end
