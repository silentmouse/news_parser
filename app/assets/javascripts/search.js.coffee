
ready = ->
  $('body').on 'click','#search_button', ->
    data = $('#search_word').val()
    time_from = $('#time_from').val()
    time_to = $('#time_to').val()
    filter = $('#filter').is(":checked")
    console.log(filter)
    $("#search_result").empty()
    $("#search_result").append('<img class="image_load" src="/images/load.GIF">')
    $.get "/search/search_word?word=" + data + "&time_from=" + time_from + "&time_to=" + time_to + "&filter=" + filter, (data)->
      $("#search_result").empty()
      $("#search_result").append(data).html


$(document).ready(ready)
$(document).on('page:load',ready)
