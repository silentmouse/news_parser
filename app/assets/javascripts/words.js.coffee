#Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


ready = ->
  $('body').on 'click','.link_date', ->
    date = $(this).attr("data-date") 
    $(".table_with_words").empty()
    $.each $(".link_date"), (i,v)->
      if $(v).hasClass("active")
        $(v).removeClass("active")
    $(this).addClass("active")
    $(".table_with_words").append('<img class="image_load" src="/images/load.GIF">')
    $.get "/words/words_in_day?date=" + date, (data)->
      $(".table_with_words").empty()
      $(".table_with_words").append(data).html
  $('body').on 'click','.category_news_in_words', ->
    id = $(this).attr("data-id")
    $.each $(".category_news_in_words"), (i,v)->
      if $(v).hasClass("active")
        $(v).removeClass("active")
    $(this).addClass("active")
    $(".div_for_empty").empty()
    date = $(".link_date.active").attr("data-date")
    $(".div_for_empty").append('<img class="image_load" src="/images/load.GIF">')
    $.get "/words/words_in_rss?category_news_id=" + id, (data)->
      $(".div_for_empty").empty()
      console.log(data)
      $(".div_for_empty").append(data).html
      

$(document).ready(ready)
$(document).on('page:load',ready)
