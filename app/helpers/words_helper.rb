module WordsHelper

  def d_m_y_for_date(date,class_in=nil,charts=false)
     str = "#{date.day}.#{date.month}.#{date.year}"
     class_res = class_in == nil ? "link_date" : "link_date #{class_in}"
     if charts != false
       link_to str, "/words/charts?time=#{date}#{charts == "" ? "" : "&category_news=#{charts}"}", class: class_res
     else
       link_to str, "javascript:void(0);", data: {date: date.to_s}, class: class_res
     end
  end

  def link_for_rss(rss)
     link_to rss.title, "javascript:void(0);", data: {title: rss.rss_index,id: rss.id}, class: "link_rss"
  end
end
