class AddColumnsToYandexNews < ActiveRecord::Migration
  def change
    add_column :yandex_news, :title, :string
    add_column :yandex_news, :count, :integer
  end
end
