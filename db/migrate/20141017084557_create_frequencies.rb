class CreateFrequencies < ActiveRecord::Migration
  def change
    create_table :frequencies do |t|
      t.datetime :date
      t.string :title
      t.integer :count

      t.timestamps
    end
  end
end
