class CreateWords < ActiveRecord::Migration
  def change
    create_table :words do |t|
      t.datetime :date
      t.string :title
      t.integer :count

      t.timestamps
    end
  end
end
