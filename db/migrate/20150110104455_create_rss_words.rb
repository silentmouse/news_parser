class CreateRssWords < ActiveRecord::Migration
  def change
    create_table :rsses_words do |t|
      t.integer :count
      t.integer :word_id
      t.integer :rss_id

      t.timestamps
    end
  end
end
