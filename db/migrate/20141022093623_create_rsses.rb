class CreateRsses < ActiveRecord::Migration
  def change
    create_table :rsses do |t|
      t.string :title
      t.string :rss_index
      t.timestamps
    end
  end
end
